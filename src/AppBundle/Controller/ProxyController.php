<?php
/**
 *
 *
 */

namespace AppBundle\Controller;

use AppBundle\Form\WeatherType;
use AppBundle\ProjectApi\Model\SearchWeather;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ProxyController extends Controller
{
    /**
     * @param Request $request
     * @Route("/proxy", name="app_proxy")
     * @return Response
     */
    public function WeatherController(Request $request){

        $searchWeather = new SearchWeather();

        $form = $this->createForm(WeatherType::class,$searchWeather);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            //$weather = $this->get('app.client.manager')->getWeatherReport($searchWeather);
            $weather  = $this->get('app.client.manager.cached')->getWeatherReport($searchWeather);
            dump($weather);
            return $this->render('Proxy/index.html.twig',array(
                'form' => $form->createView(),
                'weather' => $weather,
                'proxy' => true
            ));
        }

        return $this->render('Proxy/index.html.twig',array(
            'proxy' => true,
            'form' => $form->createView(),
        ));
    }
}