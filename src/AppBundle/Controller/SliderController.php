<?php
/**
 *
 *
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class SliderController extends Controller
{

    /**
     * @Route("/slider", name="app_slider")
     */
    public function sliderAction(){

        $projects = $this->get('app.slider.manager')->getProjects();

        dump($projects);

        return $this->render('Slider/index.html.twig',array(
            'projects' => $projects,
            'slider' => true
        ));
    }
}