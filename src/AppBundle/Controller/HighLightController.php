<?php
/**
 *
 *
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class HighLightController extends Controller
{

    /**
     * @Route("/highlight", name="app_highlighter")
     * @return Response
     */
    public function highlightAction(){

        return $this->render('HighLight/index.html.twig',array(
            'highlight' => true
        ));
    }
}