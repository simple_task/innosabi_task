<?php
namespace AppBundle\Form;

use AppBundle\ProjectApi\Model\SearchWeather;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeatherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,array(
               'label' => 'City Name'
            ))
            ->add('submit', SubmitType::class, [
                'label' => 'Search',
                'attr' => ['class' => 'btn btn-primary pull-right'],
            ]);
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => SearchWeather::class
        ));
    }

    public function getName()
    {
        return 'app_weather_form';
    }
}