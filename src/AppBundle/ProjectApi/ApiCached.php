<?php
/**
 *
 *
 */

namespace AppBundle\ProjectApi;


use AppBundle\ProjectApi\Model\WeatherInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Doctrine\Common\Cache\Cache;

/**
 * @property LoggerInterface logger
 */
class ApiCached implements ApiInterface , LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var ApiInterface $ApiClient
     */
    private $ApiClient;

    /**
     * @var Cache $cache
     */
    private $cache;

    /**
     * @var int $lifetime cache lifetime in seconds
     */
    private $lifetime;

    public function __construct(ApiInterface $ApiClient, Cache $cache, $lifetime = 300)
    {
        $this->ApiClient = $ApiClient;
        $this->cache = $cache;
        $this->lifetime = $lifetime;
    }


    /**
     * @inheritdoc
     */
    public function getWeatherReport(WeatherInterface $weather)
    {
        $hashkey = md5($weather->getName());

        if ($this->cache->contains($hashkey)) {
            $this->logger->info('Cached search result found.', ['hashkey' => $hashkey]);

            return $this->cache->fetch($hashkey);
        }

        $this->logger->info('No search result found in cache.', ['hashkey' => $hashkey]);

        $result = $this->ApiClient->getWeatherReport($weather);
        $this->cache->save($hashkey, $result, $this->lifetime);

        return $result;
    }
}