<?php


namespace AppBundle\ProjectApi;

use AppBundle\ProjectApi\Model\Weather;
use AppBundle\ProjectApi\Model\WeatherInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Promise\Promise;

/**
 * @property LoggerInterface logger
 */
class ApiManager implements ApiInterface
{
    use LoggerAwareTrait;
    /**
     * @var Client $client
     */
    private $client;

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(Client $client, $apiUrl, $apiKey)
    {
        $this->client = $client;
        $this->apiKey = $apiKey;
        $this->apiUrl = $apiUrl;
    }

    /**
     * @param WeatherInterface $weather
     * @inheritdoc
     */
    public function getWeatherReport(WeatherInterface $weather)
    {

        $this->logger->debug("initiating Weather API Calls");

        $response = array();
        try {
            /**
             * @var Promise $promise
             */
            $promise = $this->MakeApiCall($weather);
            $response = $promise->wait();
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage(), ['stacktrace' => $exception->getTraceAsString()]);
        }


        return $response;
    }

    private function MakeApiCall(WeatherInterface $weather)
    {
        $formParams = array(
            'q' => $weather->getName(),
            'appid' => $this->apiKey,
            'units' => 'metric'
        );
        $logger = $this->logger;

        $promise = $this->client->getAsync($this->apiUrl, array(
            'timeout' => 30,
            'query' => $formParams
        ))->then(function (Response $response) use ($logger) {
            $apiItems = json_decode($response->getBody()->getContents());
            $logger->debug('[Weather API] search response', array('response' => $apiItems));

            if (empty($apiItems)) {
                return $this->createEmptyPromise();
            }

            $weather = new Weather();
            $weather->setName((string)$apiItems->name);
            $description = '';
            foreach ($apiItems->weather as $weather_desc) {

                $description .= $weather_desc->main . ', ';
            }
            $weather->setId((int)$apiItems->id);
            $weather->setDescription((string)$description);
            $weather->setHumidity((float)$apiItems->main->humidity);
            $weather->setTemparature((float)$apiItems->main->temp);
            $weather->setMinTemparature((float)$apiItems->main->temp_min);
            $weather->setMaxTemparature((float)$apiItems->main->temp_max);
            $weather->setPressure((float)$apiItems->main->pressure);
            $weather->setSpeed((float)$apiItems->wind->speed);

            return $weather;
        }, function (TransferException $exception) {
            $this->logger->error(sprintf("TransferException Error Occurred %s", $exception->getMessage()));
            return $this->createEmptyPromise();
        });

        return $promise;
    }

    /**
     * @return Promise
     */
    private function createEmptyPromise()
    {
        $emptyPromise = new Promise(function () use (&$emptyPromise) {
            $emptyPromise->resolve([]);
        });

        return $emptyPromise;
    }

}