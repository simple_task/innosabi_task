<?php
/**
 * Author FarBair
 */

namespace AppBundle\ProjectApi\Model;


interface WeatherInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     */
    public function setName($name);
}