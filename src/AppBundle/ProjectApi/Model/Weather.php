<?php


namespace AppBundle\ProjectApi\Model;


class Weather implements WeatherInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $temparature;

    /**
     * @var float
     */
    private $pressure;

    /**
     * @var float
     */
    private $humidity;

    /**
     * @var float
     */
    private $min_temparature;

    /**
     * @var float
     */
    private $max_temparature;

    /**
     * @var float
     */
    private $speed;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getTemparature()
    {
        return $this->temparature;
    }

    /**
     * @param float $temparature
     */
    public function setTemparature($temparature)
    {
        $this->temparature = $temparature;
    }

    /**
     * @return float
     */
    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * @param float $pressure
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;
    }

    /**
     * @return float
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    /**
     * @param float $humidity
     */
    public function setHumidity($humidity)
    {
        $this->humidity = $humidity;
    }

    /**
     * @return float
     */
    public function getMinTemparature()
    {
        return $this->min_temparature;
    }

    /**
     * @param float $min_temparature
     */
    public function setMinTemparature($min_temparature)
    {
        $this->min_temparature = $min_temparature;
    }

    /**
     * @return float
     */
    public function getMaxTemparature()
    {
        return $this->max_temparature;
    }

    /**
     * @param float $max_temparature
     */
    public function setMaxTemparature($max_temparature)
    {
        $this->max_temparature = $max_temparature;
    }

    /**
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * @param float $speed
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;
    }
}