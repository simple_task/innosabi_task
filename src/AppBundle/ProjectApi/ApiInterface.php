<?php


namespace AppBundle\ProjectApi;

use AppBundle\ProjectApi\Model\Weather;
use AppBundle\ProjectApi\Model\WeatherInterface;

interface ApiInterface
{

    /**
     * @param WeatherInterface $weather
     * @return Weather
     */
    public function getWeatherReport(WeatherInterface $weather);
}