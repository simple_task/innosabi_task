<?php
/**
 *
 *
 */

namespace AppBundle\SliderApi;


use AppBundle\SliderApi\Model\Project;

use Psr\Log\LoggerInterface;
use Psr\Log\LoggerAwareTrait;
/**
 * @property LoggerInterface logger
 */
class Slider
{
    use LoggerAwareTrait;

    const URL = "https://demo.innosabi.com/api/v4/media/%s/thumbnail/width/1100/height/500/strategy/crop";
    /**
     * @var string
     */
    private $jsonFile;

    public function __construct($jsonFile)
    {
        $this->jsonFile = $jsonFile.'/Resources/data/projects.json';
    }

    public function  getProjects(){
        if(!file_exists($this->jsonFile)){
            throw new \Exception(sprintf("unable to find the file at the path (%s)",$this->jsonFile));
        }

        /**
         * @var Project[] $projects
         */
        $projects = [];

        $json_file  = file_get_contents($this->jsonFile);
        $jsonData = json_decode($json_file);

        foreach ($jsonData->data as $data){
            $projects [] = $this->createImageLink($data);
        }

        return $projects;
    }

    /**
     * @param $data
     * @return Project
     */
    private function createImageLink($data){

        $project = new Project();

        $url = (string)$data->image;
        $project->setUrl(sprintf(self::URL,$url));
        $project->setTitle((string)$data->name);
        $project->setDescription((string)$data->description);

        $this->logger->debug(sprintf("Slider Hash Value %s",$url));
        return $project;
    }
}